const canvas = document.querySelector("canvas");
const ctx = canvas.getContext('2d');

let bestScore = localStorage.getItem('bestScore');

if (bestScore == null){
    localStorage.setItem('bestScore', 0);
    bestScore = 0;
}

// CONFIG
 
    // Create a minimizing factor number based on screen size. Full size or 1 (factor) is what it is currently. 
    // As a screen become smaller, the factor decreases down to 375px of an iphoneX. All size related numbers will 
    // be multiplied by the rFactor = responsive factor 
   
    canvas.width = 700;
    canvas.height = 900;
    
    const screenWidth = window.screen.width;
    let scale = 1;
    let marginScale = 1;
    let canvasScale = 1;
    let marginDefault = .28;
    let marginMin = .12;

    // Below 700 and above 500, side margins decay until smallest padding. No scale implemented.
    if (screenWidth < 700 && screenWidth > 500){
        marginScale = (700 - screenWidth) / 200;
        marginScale = 1 - marginScale;
        canvas.width = 500 + (200 * marginScale);
        //scale = scale - (.1 * ((700 - screenWidth) / 200));
    } else if (screenWidth <= 500){
        // 700 is 100%, 375 is 0%
        // 69% is the max downscale for a 375px wide iPhone (.31 is inverse)
        marginScale = 0;
        scale = (500 - screenWidth) / 125;
        scale = 1 - scale;
        canvas.width = 375 + (125 * scale);
        scale = .8 + (.2 * scale);
    } else if (screenWidth >= 700){

    }

    const gridMarginTop = 133;
    const gridMarginTotalPercent = marginMin + (marginDefault * marginScale);
    const gridMarginPercent = gridMarginTotalPercent / 2;
    const gridWidth = canvas.width - (canvas.width * gridMarginTotalPercent);
    const gridInnerWidth = gridWidth / 5;
    const gridMargin = canvas.width * gridMarginPercent;
    const nextRoundButtonX = canvas.width / 2;
    const nextRoundButtonY = gridMarginTop + (gridInnerWidth / 2) + (30 * scale);

    // Bad code that covers up the scale problem. This fixes the button click x values on mobile
    let buttonLeft = nextRoundButtonX - (100 * scale);
    let buttonRight = nextRoundButtonX + (100 * scale);
    // if (screenWidth < 700){
    //     buttonLeft = nextRoundButtonX - 150;
    //     buttonRight = nextRoundButtonX + 150;
    // }

    const gridCircleRadius = 40 * scale;
    const chipRadius = 40 * scale;

    let elemHeader = document.getElementsByClassName('header')[0];
    elemHeader.style.marginLeft = `${(gridMarginPercent * 100) - 2}%`;

let fade = 0;
const bgColor = '#f7f2eb';
const activeColColor = 'rgb(100,100,100)';
const emptyChipColor = '#c5c5c5';
const chipColorInit = '#C3E2EA';
const chipColor1 = '#B4DAE4';
const chipColor2 = '#86C4D5';
const chipColor3 = '#59ADC5';
const chipColor4 = '#3A8FA6';
const chipColor5 = '#2A6879';
const chipColorPerfect = '#B3EFB2';
const chipColorOver = '#E3A1A1';
const chipColorRun = '#FFD65C';
const fontColor = '#3e3e3e';

let goalRandMin = 5; 
let goalRandMax = 15; 

let chipRandMin = 0;
let chipRandMax = 5;

let dropTime = 3000;
let chipDropSpeed = 2;
let gameOn = false;
let gameComplete = false;
let currentRound = 1;
let scoreToBeat = 200;

const points = [];
const numBubbles = [];
const awards = [];
const announcements = [];

// gameState key
// 0 = no active game, pre-start or after end pre endgame
// 1 = active game has not reached scoreToBeat
// 2 = has reached scoreToBeat, dialog is visible, game is paused
// 3 = has reached scoreToBeat, clicked continue game
// 4 = has not reached scoreToBeat, overstacked single row, gameOver
// 5 = has not reached scoreToBeat, full grid, gameOver
// 6 = has reached scoreToBeat, full grid, ready for next round
// 7 = has reached scoreToBeat, overstacked single row, move to next round || hasn't been implemented yet

let gameState = 0;

let activeCol = {
    col: 0,
    width: gridInnerWidth
};

let gridData = [[null,null,null,null,null],[null,null,null,null,null],[null,null,null,null,null],[null,null,null,null,null],[null,null,null,null,null]];
let colSums = [0,0,0,0,0];
let colGoals = [];
let chips = [];
let roundScore = 0;
let totalScore = 0;
let chipCheck;
let modalOpen = false;

function generateColGoals(min, max){
    if (colGoals.length !== 0 && gameState !== 0){
        colGoals.length = 0;
    } 
    let weight = 20 / currentRound;
    let val = 0;
    for (let i = 0; i < 5; i++){
        if (Math.floor(Math.random() * weight) == 0){
            val = Math.floor(Math.random() * (max - (max / 1.5) + 1) + (max / 1.5));
        } else {
            val = Math.floor(Math.random() * (max - min + 1) + min);
        }
        colGoals.push(val);
    }
}
generateColGoals(goalRandMin, goalRandMax);

function clearGridData(){
    for (let i = 0; i < gridData.length; i++){
        for (let j = 0; j < gridData[i].length; j++){
            gridData[i][j] = null;
        }
    }
}

function resetGame(){
    goalRandMin = 5;
    goalRandMax = 15; 
    chipRandMin = 0;
    chipRandMax = 5;
    dropTime = 3000;
    chipDropSpeed = 2;
    gameOn = false;
    gameComplete = false;
    currentRound = 1;
    scoreToBeat = 200;
    gridData = [[null,null,null,null,null],[null,null,null,null,null],[null,null,null,null,null],[null,null,null,null,null],[null,null,null,null,null]];
    colSums = [0,0,0,0,0];
    chips = [];
    roundScore = 0;
    totalScore = 0;
    generateColGoals(goalRandMin, goalRandMax);
    bestScore = localStorage.getItem('bestScore');
    awards.splice(0,awards.length);
    announcements.forEach(announcement => {
        announcement.announced = false;
    })
    let modal = document.getElementsByClassName("modal")[0];
    if (modal.style.display == 'none'){
        modalOpen = false;
    }
}

function drawGrid(activeCol) {

    ctx.fillStyle = bgColor; // Background Color
    ctx.fillRect(0,0,canvas.width, canvas.height); 

    ctx.lineWidth = 1;
    // DRAW TITLE
    ctx.textAlign = 'left';
    ctx.fillStyle = '#000';
    ctx.font = `${50 * scale}px 'Bebas Neue'`;
    ctx.fillText("drippy", gridMargin, (40 * scale));

    // DRAW ACTIVE COLUMN
    ctx.fillStyle = activeColColor; // Active Column
    ctx.fillRect((activeCol.col * gridInnerWidth + gridMargin), gridMarginTop, activeCol.width, gridInnerWidth * 5);

    // DRAW BEST SCORE
    ctx.fillStyle = "rgba(255, 255, 0, .5)";
    ctx.strokeStyle = 'rgba(0,0,0,1)';
    roundRect(ctx, canvas.width - gridMargin - (130 * scale), (2 * scale), (130 * scale), (45 * scale), 3, true);
    ctx.font = `${15 * scale}px 'Barlow Condensed'`;
    ctx.fillStyle = fontColor;
    ctx.textAlign = 'center';
    ctx.fillText("BEST", canvas.width - gridMargin - (68 * scale), (16 * scale));
    ctx.textAlign = 'center';
    ctx.font = `${30 * scale}px 'Barlow Condensed'`;
    if (bestScore >= (totalScore + roundScore)){
        ctx.fillText(bestScore, canvas.width - gridMargin - (68 * scale), (41 * scale));
    } else {
        ctx.fillText(totalScore, canvas.width - gridMargin - (68 * scale), (41 * scale));
    }

    // DRAW Game SCORE
    ctx.fillStyle = "rgba(255, 255, 0, .5)";
    roundRect(ctx, canvas.width - gridMargin - (270 * scale), (2 * scale), (130 * scale), (45 * scale), 3, true);
    ctx.font = `${15 * scale}px 'Barlow Condensed'`;
    ctx.fillStyle = fontColor;
    ctx.textAlign = 'center';
    ctx.fillText("GAME", canvas.width - gridMargin - (205 * scale), (16 * scale));
    ctx.textAlign = 'center';
    ctx.font = `${30 * scale}px 'Barlow Condensed'`;
    ctx.fillText(totalScore, canvas.width - gridMargin - (205 * scale), (41 * scale));
  
    // DRAW Round SCORE
    ctx.fillStyle = "rgba(255, 255, 0, .5)";
    roundRect(ctx, canvas.width - gridMargin - (130 * scale), (55 * scale), (130 * scale), (45 * scale), 3, true);
    ctx.font = `${15 * scale}px 'Barlow Condensed'`;
    ctx.fillStyle = fontColor;
    ctx.textAlign = 'center';
    ctx.fillText(`ROUND: ${currentRound}`, canvas.width - gridMargin - (68 * scale), (70 * scale));
    ctx.textAlign = 'center';
    ctx.font = `${25 * scale}px 'Barlow Condensed'`;
    ctx.fillText(`${roundScore} / ${scoreToBeat}`, canvas.width - gridMargin - (68 * scale), (92 * scale));

    // DRAW AWARDS 
    awards.forEach((award) => {
        award.draw();
    });

    // DRAW EMPTY CIRCLES
    for (let j = 0; j < 5; j++){ // draw columns circles
        let circleHeight = ((j * gridInnerWidth) + (gridInnerWidth / 2) + (gridMarginTop));
        for (let i = 0; i < 5; i++){ // draw rows circles
            let circleRow = ((i * gridInnerWidth) + (gridInnerWidth / 2) + gridMargin);

            ctx.fillStyle = emptyChipColor;
            ctx.beginPath();
            ctx.arc(circleRow,circleHeight,gridCircleRadius,0, Math.PI * 2, true);
            ctx.fill();
        }
    }

    // DRAW LINE UNDER Chips
    ctx.beginPath();
    ctx.moveTo(gridMargin, (gridWidth + gridMarginTop + (5 * scale)));
    ctx.lineTo((gridMargin + gridWidth),(gridWidth + gridMarginTop + (5 * scale)));
    ctx.closePath();
    ctx.stroke();

    // DRAW SUMMED Rectangles
    for (let i = 0; i < 5; i++){
        let colHeight = ((i * gridInnerWidth) + gridMargin + (gridInnerWidth * .1));
        let rowHeight = gridMarginTop + gridWidth + (15 * scale);
        ctx.fillStyle = '#f1c9b2';
        roundRect(ctx, colHeight, rowHeight, (gridInnerWidth * .8), (30 * scale), 2, true, false);
    }
    
    // DRAW SUMS
    for (let i = 0; i < 5; i++){
        let colHeight = ((i * gridInnerWidth) + (gridInnerWidth / 2) + gridMargin);
        let rowHeight = gridMarginTop + gridWidth + (39 * scale);
        ctx.fillStyle = fontColor;
        ctx.textAlign = 'center';
        ctx.font = `${25 * scale}px 'Barlow Condensed'`;
        ctx.fillText(colSums[i], colHeight, rowHeight);
    }

    // DRAW GOALS
    for (let i = 0; i < 5; i++){
        let colHeight = ((i * gridInnerWidth) + gridMargin + (gridInnerWidth * .1));
        let rowHeight = gridMarginTop + gridWidth + (50 * scale);
        ctx.fillStyle = '#d96e30';
        roundRect(ctx, colHeight, rowHeight, (gridInnerWidth * .8), (40 * scale), 2, true, false);
        colHeight = ((i * gridInnerWidth) + (gridInnerWidth / 2) + gridMargin);
        rowHeight = gridMarginTop + gridWidth + (78 * scale);
        ctx.fillStyle = '#eee';
        ctx.textAlign = 'center';
        ctx.font = `${25 * scale}px 'Barlow Condensed'`;
        ctx.fillText(colGoals[i], colHeight, rowHeight);
    }

    // DRAW CHIPS
    chips.forEach((chip) => {
        chip.draw();
    });

    // DRAW ANIMATED NUMBER BUBBLING
    if(numBubbles.length > 0){
        for (let i = numBubbles.length - 1; i > -1; i--){
            if(numBubbles[i].alpha <= 0){
                numBubbles.splice(i, 1);
            }
        } 
    }
    numBubbles.forEach(bubble => bubble.draw());

    // DRAW ANNOUNCEMENTS
    announcements.forEach((announcement) => {
        if(announcement.active == true){
            announcement.draw();
        }
    });


}

function drawModal(heading, text, buttons){
    if (fade < 100){
        fade = fade + 2;    
    }
    ctx.fillStyle = `rgba(0,0,0,${.6*(fade/100)})`;
    roundRect(ctx, gridMargin - (20 * scale), gridMarginTop - (20 * scale), (gridInnerWidth * 5) + (35 * scale), (gridInnerWidth * 5) + (30 * scale), 5, true, false);

    // HEADING    
    if (heading !== undefined){
        ctx.fillStyle = '#eee';
        ctx.textAlign = 'center';
        ctx.font = `${50 * scale}px 'Bebas Neue'`;
        ctx.fillText(heading, canvas.width / 2, gridMarginTop + (gridInnerWidth / 2) + (10 * scale));
    }

    // BUTTONS
    points.forEach(point => point.draw());

    if (buttons == 1){
        if (points.length === 0){
            points.push(new HoverPoint(nextRoundButtonX - (100 * scale), nextRoundButtonY, (200 * scale), (45 * scale), '#D96E30', '#DF8653'));
        }
        ctx.fillStyle = '#333';
        ctx.textAlign = 'center';
        ctx.font = `${25 * scale}px 'Barlow Condensed'`;
        if (gameState === 0){
            ctx.fillText('START GAME', nextRoundButtonX, nextRoundButtonY + (32 * scale));
        } else if (gameState === 4 || gameState === 5){
            ctx.fillText('NEW GAME', nextRoundButtonX, nextRoundButtonY + (32 * scale));
        } else {
            ctx.fillText('NEXT ROUND', nextRoundButtonX, nextRoundButtonY + (32 * scale));
        }
    } else if (buttons == 2){
        if (points.length === 0){
            points.push(new HoverPoint(nextRoundButtonX - (100 * scale), nextRoundButtonY, (200 * scale), (45 * scale), '#D96E30', '#DF8653'));
            points.push(new HoverPoint(nextRoundButtonX - (100 * scale), nextRoundButtonY + (70 * scale), (200 * scale), (45 * scale), '#E69E75', '#ECB798'));
        }
        ctx.fillStyle = '#333';
        ctx.textAlign = 'center';
        ctx.font = `${25 * scale}px 'Barlow Condensed'`;
        ctx.fillText('NEXT ROUND', nextRoundButtonX, nextRoundButtonY + (32 * scale));
        ctx.fillStyle = '#333';
        ctx.textAlign = 'center';
        ctx.font = `${25 * scale}px 'Barlow Condensed'`;
        ctx.fillText('KEEP PLAYING', nextRoundButtonX, nextRoundButtonY + (102 * scale));
    } 

    // TEXT
    if (text !== undefined){
        ctx.fillStyle = '#eee';
        ctx.textAlign = 'center';
        ctx.font = `${20 * scale}px 'Barlow Condensed'`;
        let textX = canvas.width / 2;
        let textY = gridMarginTop + (175 * scale);
        let lineHeight = (20 * scale);
        let lines = text.split('\n');
        for (let i = 0; i < lines.length; i++){
            ctx.fillText(lines[i], textX, textY + (i*lineHeight));
        }
    }
}

function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
 if (typeof stroke === 'undefined') {
   stroke = true;
 }
 if (typeof radius === 'undefined') {
   radius = 5;
 }
 if (typeof radius === 'number') {
   radius = {tl: radius, tr: radius, br: radius, bl: radius};
} else {
    var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
    for (var side in defaultRadius) {
      radius[side] = radius[side] || defaultRadius[side];
    }
  }
  let path = new Path2D();
  path.moveTo(x + radius.tl, y);
  path.lineTo(x + width - radius.tr, y);
  path.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
  path.lineTo(x + width, y + height - radius.br);
  path.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
  path.lineTo(x + radius.bl, y + height);
  path.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
  path.lineTo(x, y + radius.tl);
  path.quadraticCurveTo(x, y, x + radius.tl, y);
  if (fill) {
    ctx.fill(path);
  }
  if (stroke) {
    ctx.stroke(path);
  }
  return path;
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? `${parseInt(result[1], 16)},${parseInt(result[2], 16)},${parseInt(result[3], 16)}` : null;
}

class Point {
    constructor(x, y, w, h){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }
}

class HoverPoint extends Point {
    constructor(x, y, w, h, color, hoverColor){
        super(x, y, w, h);
        this.color = color;
        this.hoverColor = hoverColor;
        this.hovered = false;
        this.path = new Path2D();
    }

    draw(){
        this.hovered ? ctx.fillStyle = this.hoverColor : ctx.fillStyle = this.color;
        this.path = roundRect(ctx, this.x, this.y, this.w, this.h, 5, true, false);
    }
}

class Cursor extends Point {
    constructor(x, y, w, h){
        super(x, y, w, h);
    }
    collisionCheck(points){
        document.body.style.cursor = "default";
        points.forEach(point => {
            point.hovered = false;
            if (ctx.isPointInPath(point.path, this.x, this.y)){
                document.body.style.cursor = "pointer";
                point.hovered = true;
            }
        });
    }
}

class NumBubbleUp {
    constructor(x, y, val, color, size, alphaStep){
        this.x = x;
        this.y = y;
        this.color = hexToRgb(color);
        this.size = size;
        this.alphaStep = alphaStep;
        this.alpha = 1;
        if (val > 0){
            this.val = `+${val}`;
        } else {
            this.val = val;
        }
    }

    draw(){
        ctx.font = `${this.size}px 'Barlow Condensed'`;
        ctx.fillStyle = `rgba(${this.color},${this.alpha})`;
        ctx.textAlign = 'center';
        ctx.fillText(this.val, this.x, this.y);
        this.y = this.y - 2;
        this.size = this.size + 1;
        this.alpha = this.alpha - this.alphaStep;
    }
}

class Announcement {
    constructor(x, y, msg){
        this.x = x;
        this.y = y - gridInnerWidth;
        this.msg = msg;
        this.size = 50 * scale;
        this.color = "0,0,0";
        this.alpha = 1
        this.alphaStep = .02;
        this.announced = false;
        this.active = false;
    }

    draw(){
        ctx.font = `${this.size}px 'Bebas Neue'`;
        ctx.fillStyle = `rgba(${this.color},${this.alpha})`;
        ctx.textAlign = 'center';
        ctx.fillText(this.msg, this.x, this.y);
        this.size = this.size + 1;
        this.alpha = this.alpha - this.alphaStep;

        if(this.alpha <= 0){
            this.active = false;
            this.announced = true;
        }
    }

    announce(){
        this.active = true;
    }
}

// ANNOUNCEMENT KEY
// 0 = SANDWICH
// 1 = RUN
const msg1 = new Announcement(canvas.width / 2, canvas.height / 2, "SANDWICH UNLOCKED");
announcements.push(msg1);
const msg2 = new Announcement(canvas.width / 2, canvas.height / 2, "STRAIGHT UNLOCKED");
announcements.push(msg2);
const msg3 = new Announcement(canvas.width / 2, canvas.height / 2, "RUN UNLOCKED");
announcements.push(msg3);

class Awards {
    constructor(x, y, w, h, color, value, column){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h; 
        this.color = color;
        this.value = value;
        this.col = column;
    }

    draw(){
        ctx.fillStyle = this.color;
        roundRect(ctx, this.x, this.y, this.w, this.h, (20 * scale), true, false);
    }
}

class Chip {
    constructor(x, y, radius, color, value){
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.creationTime = new Date().getTime();
        this.value = value;
        this.active = true;
        this.dropped = false;
        this.forceDrop = false;
        this.currentRow = 0;
        this.maxY = null;
        this.finalCol = null;
        this.ringSize = radius + 3;
        this.ringAlpha = 1;
        this.ringTime = new Date().getTime();

    }

    draw(){

        let now = new Date().getTime();
        let pulse = dropTime / 3;

        if (this.active){
            // Set X value based on active column
            this.x = (activeCol.col * gridInnerWidth) + gridMargin + (gridInnerWidth / 2); 

            checkColumnDepth(this);

            if (this.y >= this.maxY && this.dropped == true){
                this.active = false;
                gridData[activeCol.col][this.currentRow] = this.value;
                sumColumns(this);
            }
            
            if (this.forceDrop){
                if (this.maxY - this.y < 10){
                    this.y = this.maxY;
                } else if (this.maxY - this.y >= 10){
                    this.y = this.y + 10;
                }
            }
    
        }
        
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
        ctx.fillStyle = this.color;
        ctx.fill();

        ctx.textAlign = 'center';
        ctx.font = `${30 * scale}px 'Bebas Neue'`;
        ctx.fillStyle = "#333";
        ctx.fillText(this.value, this.x, this.y + (8 * scale));

        if (this.active){
            if ((now - this.creationTime) < dropTime && !this.dropped){
                if ((now - this.ringTime) < pulse){
                    ctx.beginPath();
                    ctx.arc(this.x, this.y, this.ringSize, 0, Math.PI * 2, true);
                    ctx.strokeStyle = `rgba(0,0,0,${this.ringAlpha})`;
                    ctx.lineWidth = 1;
                    ctx.stroke();
                    this.ringAlpha = this.ringAlpha - .03;
                } else {
                    this.ringTime = new Date().getTime();
                    this.ringAlpha = 1;
                }

            } else { // after 3 pulses start drop
                this.dropped = true;
                if (this.maxY - this.y < chipDropSpeed){
                    this.y = this.maxY;
                } else if (this.maxY - this.y >= chipDropSpeed){
                    this.y = this.y + chipDropSpeed;
                }
            }
        }
    }

    drop(){
        this.forceDrop = true;
        this.dropped = true;
        checkFullColumn();
    }
}

function nextRound(key){
    // key is a number passed into nextRound depending on scenario
    // 1 is a click on the next round button, or start over for gameState 5, starts new game
    // 2 is a continue current round
    // 3 is 

    if (key === 1){
        dataLayer.push({'event':'startGame'});
        resetGame();
        gameOn = true;
        gameState = 1;
        chipCheck = setInterval(timer, 100);
    } 
    else if (key === 3){
        totalScore = totalScore + roundScore;
        roundScore = 0;
        currentRound++;
        dataLayer.push({'event':'currentRound', 'currentRound': currentRound});
        scoreToBeat = scoreToBeat + 100;
        chipDropSpeed++;
        goalRandMin = goalRandMin + 1;
        goalRandMax = goalRandMax + 2; 
        chipRandMin = 0;
        chipRandMax = chipRandMax + 1;
        dropTime = dropTime - 200;
        clearGridData();
        awards.splice(0,awards.length);
        colSums = [0,0,0,0,0];
        colGoals.length = 0;
        generateColGoals(goalRandMin, goalRandMax);
        chips = [];
        gameOn = true;
        gameState = 1;
        chipCheck = setInterval(timer, 100);
    }
    else if (key === 2 || gameState === 2){
        gameOn = false;
        let heading = "YOU WON THIS ROUND";
        drawModal(heading,undefined, 2);
    }
    else if (gameState === 0) {
        let text = "Drop the numbered chip into the desired column using\n the arrow keys or swiping. Try to match your column total\n to the column goals in the orange boxes below.\n Get extra points for perfect columns,\n move on to higher levels,\n and unlock special multipliers like sandwiches and runs!";
        let heading = "NEW GAME";
        drawModal(heading, text, 1);
    }
    else if (gameState === 6){
        gameOn = false;
        let heading = `YOU BEAT ROUND ${currentRound}!`;
        drawModal(heading, undefined, 1);
    }
    else if (gameState === 4){
        endGame(true);
    }
    else if (gameState === 5){
        endGame(true);
    }
}

function timer(){
    if(!checkActiveChips() && gameOn){
        createChip();
    }
}

function createChip(){
    // create random chip value
    let weight = currentRound * 4;
    let weightedRange = currentRound + 3;
    let val = 0;
    if (Math.floor(Math.random() * weight) == 0){
        val = Math.floor(Math.random() * weightedRange);
    } else {
        val = Math.floor(Math.random() * (chipRandMax - chipRandMin) + chipRandMin);
    }
    
    const chip = new Chip((gridMargin + activeCol.col * gridInnerWidth), (gridMarginTop - gridInnerWidth / 2), chipRadius, chipColorInit, val);
    chips.push(chip);
    chip.draw();
}

function animate(){
    requestAnimationFrame(animate);
    ctx.clearRect(0,0,canvas.width, canvas.height);
    drawGrid(activeCol);

    if (checkActiveChips() && gameOn){
        checkFullColumn();
    }

    if (!gameOn){
        nextRound();
    }
}

function checkActiveChips (){
    let active = false;
    chips.forEach((chip) => {
        if (chip.active){
            active = true;
        }
    })
    return active;
}

function checkColumnDepth (chip){
    chip.maxY = (4 * gridInnerWidth) + gridMarginTop + (gridInnerWidth / 2);
    let emptyCol = true;
    for (let i = 0; i < 5; i++){
        if(gridData[activeCol.col][i] !== null){
            depth = (i * gridInnerWidth) + gridMarginTop - (gridInnerWidth / 2);
            chip.currentRow = i - 1;
            chip.maxY = depth;
            emptyCol = false;
            break;
        }
    }
    if (emptyCol){
        depth = (5 * gridInnerWidth) + gridMarginTop - (gridInnerWidth / 2);
        chip.currentRow = 4;
        chip.maxY = depth;
    }
}

function checkFullColumn (){
    if(gridData[activeCol.col][0] !== null && chips[chips.length - 1].dropped == true){
        if (gameState === 3){
            gameState = 6;
            nextRound();
        } else {
            gameState = 4;
            totalScore = totalScore + roundScore;
            endGame(true);
        }
    }
}

function checkFullGrid() {
    if(gridData[activeCol.col][0] !== null){
        let fullCols = 0;
        for (let i = 0; i < 5; i++){
            if (gridData[i][0] !== null){
                fullCols++;
            }
        }
        if (fullCols == 5){
            return true;
        } else {
            return false;  
        }
    }
}

function endGame(status){

    // Is only true the first time through
    if (gameOn){
        if (bestScore <= totalScore){
            localStorage.removeItem('bestScore');
            localStorage.setItem('bestScore', totalScore);
        }
        dataLayer.push({'event':'finalScore', 'finalScore': totalScore});
    }

    if (!modalOpen){
        let modalMessage = `You scored ${totalScore} points and got to level ${currentRound}!`;
        document.getElementsByClassName("modal-score")[0].textContent = modalMessage;
        let encodedMSG = encodeURIComponent(`I scored ${totalScore} points and got to level ${currentRound}! Try to beat my Drippy score!`)
        let twitterMessage = `https://twitter.com/intent/tweet?url=https://puppiesvsrobots.com&text=${encodedMSG}&hashtags=drippy`;
        document.getElementById('twitter-btn').setAttribute('href', twitterMessage);
        document.getElementsByClassName("modal")[0].style.display = "block";
        window.twttr = (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0],
              t = window.twttr || {};
            if (d.getElementById(id)) return t;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
          
            t._e = [];
            t.ready = function(f) {
              t._e.push(f);
            };
          
            return t;
          }(document, "script", "twitter-wjs"));
        modalOpen = true;
    }

    // IF statement was for a full grid win but didn't reach the scoreToBeat compared to a loss by overfilled column, I don't know if this is necessary and at the moment they act the same. 
    if(status){
        let heading = "GAME OVER";
        let text = `YOU MADE IT TO ROUND ${currentRound}\nWITH A HIGH SCORE OF ${totalScore}`;
        drawModal(heading, text, 1);
    } else {
        console.log("GAME OVER, BAD JOB!");
    }
    gameOn = false;
    clearInterval(chipCheck);

    
}

function sumColumns (chip){
    colSums[activeCol.col] = colSums[activeCol.col] + chip.value;
    chip.finalCol = activeCol.col;
    roundScore = roundScore + chip.value;
    const numBubble = new NumBubbleUp(canvas.width - gridMargin - (95 * scale), (85 * scale), chip.value, '#444', (15 * scale), .05);
    numBubbles.push(numBubble);

    //If full column and colSum equals colGoal
    if(gridData[activeCol.col][0] !== null && colSums[activeCol.col] == colGoals[activeCol.col]){
        roundScore = roundScore + 100;
        let x = ((activeCol.col * gridInnerWidth) + gridMargin + (gridInnerWidth / 2) - (10 * scale));
        let y = gridMarginTop + gridWidth + (25 * scale);
        const numBubble = new NumBubbleUp(x, y, 100, '#000000', (40 * scale), .03);
        numBubbles.push(numBubble);

        // Clear column in gridData & goal in colGoals & Sum in colSums
        for (let i = 0; i < 5; i++){
            gridData[activeCol.col][i] = null;
        }

        // Iterate through chips and remove those that are in the perfect column
        for (let i = chips.length - 1; i > -1; i--){
            if(chips[i].finalCol == activeCol.col){
                chips.splice(i, 1);
            }
        }   

        // Remove awards from column
        let i = awards.length;
        while (i--){
            if(awards[i].col === activeCol.col){
                awards.splice(i, 1);
            }
        }

        colGoals[activeCol.col] = 0;
        colSums[activeCol.col] = 0;
        // Generate new random goal and insert into colGoals
        colGoals[activeCol.col] = Math.floor(Math.random() * (goalRandMax - goalRandMin + 1) + goalRandMin);
    }

       // Active Column can't be full to give awards
    if (chip.currentRow >= 0){
        // Detect Sandwich
        if((totalScore + roundScore) >= 1000){
            if(chip.value == gridData[activeCol.col][chip.currentRow + 2] && chip.value !== gridData[activeCol.col][chip.currentRow + 1]){
                roundScore = roundScore + 200;
                const award = new Awards((activeCol.col * gridInnerWidth) + gridMargin, chip.y - (gridInnerWidth / 2), gridInnerWidth, gridInnerWidth * 3, '#EEC4BE', 200, activeCol.col);
                awards.push(award);
                const numBubble = new NumBubbleUp((activeCol.col * gridInnerWidth) + gridMargin + (gridInnerWidth / 2), chip.y + gridInnerWidth, 200, '#444', (30 * scale), .05);
                numBubbles.push(numBubble);
            }
        }

        // Detect Run
        if ((totalScore + roundScore) >= 3000){
            if(chip.value == gridData[activeCol.col][chip.currentRow + 1]){
                if(chip.value == gridData[activeCol.col][chip.currentRow + 2]){
                    roundScore = roundScore + 400;
                    const award = new Awards((activeCol.col * gridInnerWidth) + gridMargin, chip.y - (gridInnerWidth / 2), gridInnerWidth, gridInnerWidth * 3, '#BCF2BB', 400, activeCol.col);
                    awards.push(award);
                    const numBubble = new NumBubbleUp((activeCol.col * gridInnerWidth) + gridMargin + (gridInnerWidth / 2), chip.y + gridInnerWidth, 400, '#444', (30 * scale), .05);
                    numBubbles.push(numBubble);
                }
            }
        }

        // Detect Straight
        if ((totalScore + roundScore) >= 5000 && chip.currentRow < 3){
            if(chip.value == (gridData[activeCol.col][chip.currentRow + 1] + 1) && chip.value == (gridData[activeCol.col][chip.currentRow + 2] + 2)){
                roundScore = roundScore + 600;
                const award = new Awards((activeCol.col * gridInnerWidth) + gridMargin, chip.y - (gridInnerWidth / 2), gridInnerWidth, gridInnerWidth * 3, '#FFAC70', 1000, activeCol.col);
                awards.push(award);
                const numBubble = new NumBubbleUp((activeCol.col * gridInnerWidth) + gridMargin + (gridInnerWidth / 2), chip.y + gridInnerWidth, 400, '#444', (30 * scale), .05);
                numBubbles.push(numBubble);
            } else if (chip.value == (gridData[activeCol.col][chip.currentRow + 1] - 1) && chip.value == (gridData[activeCol.col][chip.currentRow + 2] - 2)){
                roundScore = roundScore + 1000;
                const award = new Awards((activeCol.col * gridInnerWidth) + gridMargin, chip.y - (gridInnerWidth / 2), gridInnerWidth, gridInnerWidth * 3, '#FFAC70', 1000, activeCol.col);
                awards.push(award);
                const numBubble = new NumBubbleUp((activeCol.col * gridInnerWidth) + gridMargin + (gridInnerWidth / 2), chip.y + gridInnerWidth, 400, '#444', (30 * scale), .05);
                numBubbles.push(numBubble);
            } 
        }
    }

    // subtracts difference between goal and col sum from roundScore
    if(gridData[activeCol.col][0] !== null){
        roundScore = roundScore - Math.abs(colGoals[activeCol.col] - colSums[activeCol.col]);
        let x = ((activeCol.col * gridInnerWidth) + gridMargin + (gridInnerWidth / 2) - (10 * scale));
        let y = gridMarginTop + gridWidth + (25 * scale);
        const numBubble = new NumBubbleUp(x, y, -1 * Math.abs(colGoals[activeCol.col] - colSums[activeCol.col]), '#C74343', (40 * scale), .03);
        numBubbles.push(numBubble);    
    }

    // Check for Rewards to unlock
    if(totalScore + roundScore >= 1000){
        // Annouce Sandwich
        if(!announcements[0].announced){
            announcements[0].announce();
        }
    } 
    if (totalScore + roundScore >= 3000){
        // Announce Run
        if(!announcements[1].announced){
            announcements[1].announce();
        }
    }
    if (totalScore + roundScore >= 5000){
        // Announce Straight
        if(!announcements[2].announced){
            announcements[2].announce();
        }
    }

    // COLOR chip column based on proximity to goal
    let chipDif = colGoals[activeCol.col] - colSums[activeCol.col];
    if (chipDif > 0){
        let chipRanges = colGoals[activeCol.col] / 5;
        let chipColorNum = Math.floor(colSums[activeCol.col] / chipRanges);
        if (chipColorNum == 0){
            chipColorNum++;
        }
        let newChipColor = chipColor1;
        switch (chipColorNum){
            case 1:
                newChipColor = chipColor1;
                break;
            case 2:
                newChipColor = chipColor2;
                break;
            case 3:
                newChipColor = chipColor3;
                break;
            case 4:
                newChipColor = chipColor4;
                break;
            case 5:
                newChipColor = chipColor5;
                break;
        }
        for (let i = 0; i < chips.length; i++){
            if(chips[i].finalCol == activeCol.col){
                chips[i].color = newChipColor;
            }
        }
    } else if (chipDif == 0){
        for (let i = 0; i < chips.length; i++){
            if(chips[i].finalCol == activeCol.col){
                chips[i].color = chipColorPerfect;
            }
        }
    } else {
        for (let i = 0; i < chips.length; i++){
            if(chips[i].finalCol == activeCol.col){
                chips[i].color = chipColorOver;
            }
        }
    }

    if (roundScore >= scoreToBeat && gameState == 1){
        // If advance to the next level has not been asked, bring up the option
        gameState = 2;
        nextRound(2);
    }

    if (checkFullGrid()){
        if (roundScore < scoreToBeat){
            totalScore = totalScore + roundScore;
            gameState = 5;
            endGame(true);
        }
        else {
            gameState = 6;
            nextRound();
        }
    }
}

function updateActiveColumn(keystroke) {

    if(chips[chips.length - 1].active == true && (chips[chips.length - 1].dropped == false)){
        // chip is active but hasn't dropped, allow for movement left and right
        if (keystroke === "ArrowLeft" && activeCol.col > 0){
            activeCol.col = activeCol.col - 1;
        }
        if (keystroke === "ArrowRight" && activeCol.col < 4){
            activeCol.col = activeCol.col + 1;
        }
    }

    if(chips[chips.length - 1].active == true && chips[chips.length - 1].dropped == true){
        // changed gridMargin to gridMarginTop in below line
        let currentDroppingRow = Math.trunc((chips[chips.length-1].y + chips[chips.length-1].radius - gridMarginTop) / gridInnerWidth);
        if (keystroke === "ArrowLeft" && activeCol.col > 0){
            if (gridData[(activeCol.col - 1)][currentDroppingRow] !== null){
            } else {
                activeCol.col = activeCol.col - 1;
            }
        }
        if (keystroke === "ArrowRight" && activeCol.col < 4){
            if (gridData[(activeCol.col + 1)][currentDroppingRow] !== null){
            } else {
                activeCol.col = activeCol.col + 1;
            }
        }
    }
}

function getMousePos(evt) {
    let rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

addEventListener('keydown', (event) => {
    if (event.key === "ArrowDown" || event.key === "ArrowLeft" || event.key === "ArrowRight"){
        event.preventDefault();
    }
    if (event.key == "ArrowDown" && gameOn){
        event.preventDefault();
        if(checkActiveChips()){
            chips[chips.length-1].drop();
        }
    }
    if ((event.key === "ArrowLeft" || event.key === "ArrowRight") && gameOn){
        event.preventDefault();
        updateActiveColumn(event.key);
    }
});

addEventListener('click', (event) => {
    let click = getMousePos(event);
    if (gameState == 0 || gameState == 5 || gameState == 4){
        if (click.x > buttonLeft && click.x < buttonRight){
            if (click.y > nextRoundButtonY && click.y < nextRoundButtonY + (45 * scale)){
                points.splice(0, points.length);
                nextRound(1);
            }
        }
    }
    if (gameState == 2){
        if (click.x > buttonLeft && click.x < buttonRight){
            if (click.y > nextRoundButtonY && click.y < nextRoundButtonY + (45 * scale)){
                points.splice(0, points.length);
                nextRound(3);
            }
            if (click.y > nextRoundButtonY + (70 * scale) && click.y < nextRoundButtonY + (115 * scale)){
                points.splice(0, points.length);
                gameState = 3;
                gameOn = true;
            }
        }
    }
    if (gameState == 6){
        if (click.x > buttonLeft && click.x < buttonRight){
            if (click.y > nextRoundButtonY && click.y < nextRoundButtonY + (50 * scale)){
                nextRound(3);
                points.splice(0, points.length);
            }
        }
    }
});

addEventListener('mousemove', (event) => {
    const cursor = new Cursor(event.offsetX, event.offsetY);
    cursor.collisionCheck(points);
});

// MOBILE GESTURE SUPPORT
function swipedetect(el, callback){
  
    var touchsurface = el,
    swipedir,
    startX,
    startY,
    distX,
    distY,
    threshold = 60, //required min distance traveled to be considered swipe
    restraint = 100, // maximum distance allowed at the same time in perpendicular direction
    allowedTime = 500, // maximum time allowed to travel that distance
    elapsedTime,
    startTime,
    handleswipe = callback || function(swipedir){}
  
    touchsurface.addEventListener('touchstart', function(e){
        var touchobj = e.changedTouches[0]
        swipedir = 'tap'
        dist = 0
        startX = touchobj.pageX
        startY = touchobj.pageY
        startTime = new Date().getTime() // record time when finger first makes contact with surface
        e.preventDefault()
    }, false)
  
    touchsurface.addEventListener('touchmove', function(e){
        e.preventDefault() // prevent scrolling when inside DIV
    }, false)
  
    touchsurface.addEventListener('touchend', function(e){
        var touchobj = e.changedTouches[0]
        distX = touchobj.pageX - startX // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY // get vertical dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime // get time elapsed
        if (elapsedTime <= allowedTime){ // first condition for awipe met
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ // 2nd condition for horizontal swipe met
                swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ // 2nd condition for vertical swipe met
                swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
            }
        }
        handleswipe(swipedir, startX, startY)
        e.preventDefault()
    }, false)
}
  
let el = document.querySelector('canvas');
swipedetect(el, function(swipedir, startX, startY){
    let tapPercent = startX / canvas.width;
    if (swipedir === "down" && gameOn){
        if(checkActiveChips()){
            chips[chips.length-1].drop();
        }
    }
    if (swipedir === "tap" && gameOn ){
        if (tapPercent > .5){
            updateActiveColumn("ArrowRight");
        }
        if (tapPercent < .5){
            updateActiveColumn("ArrowLeft");
        }
    }
    
    if (swipedir === "tap"){
        const event = new MouseEvent('click', {
            'clientX': startX,
            'clientY': startY
        });
        
        window.dispatchEvent(event);
        
    }
});

animate();